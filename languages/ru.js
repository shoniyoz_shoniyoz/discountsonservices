export default {
  header: {
    faq: 'Ч.з.в',
    support: 'Поддерживать',
    about: 'О',
  },
  main: {
    h1: 'Начните пользоваться выгодой до 50%',
    p: 'Вы всегда хотели получить такой же товар по специальной для вас цене, не торгуясь - и он ваш.',
    button: 'Начать использовать',
  },
}
