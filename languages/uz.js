export default {
  header: {
    faq: 'KSS',
    support: "Qo'llab-quvvatlash",
    about: 'Haqida',
  },
  main: {
    h1: '50% gacha imtiyozdan foydalanishni boshlang',
    p: "Siz har doim bir xil mahsulotni o'zingiz uchun maxsus narxda, savdolashmasdan olishni xohlagansiz - va bu sizniki.",
    button: 'Foydalanishni boshlang',
  },
}
