export default {
  header: {
    faq: 'FAQ',
    support: 'Support',
    about: 'About',
  },
  main: {
    h1: 'Start enjoying a benefit of up to 50%',
    p: 'You have always wanted to get the same product at a special price for you, without haggling - and it is yours.',
    button: 'Start using',
  },
}
